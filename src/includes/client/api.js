"use strict";

$(document).ready(function()
{
	var api = new API();
	api.WebSocket();
});

function API()
{
	
	var obj = this;
	
	this.url = "www.altcointrader.co.za/api/v3/";
	this.socket = null;
	this.interval;
	this.time = 10000;
	
	// push
	this.WebSocket = function()
    {
		if ("WebSocket" in window)
		{
			this.socket = new WebSocket("wss://" + obj.url);
	
			this.socket.onopen = function()
			{};
			
			this.socket.onclose = function()
			{};
			
			this.socket.onmessage = function (event) 
			{
				obj.Boards(event.data);
			};
	
			this.socket.onerror = function(event)
			{
				$("#error").html("Error connecting to WebSocket, Reverting to Pull");
				
				obj.Pull();
			}
			
			window.onbeforeunload = function(event) 
			{
				if (this.socket)
				{
					this.socket.close();
				}
			};
		}
		else
		{
			// fallback to pull requests if not supported by browser
			obj.Pull();
		}
    }
	
	// pull
	this.Pull = function()
	{
		obj.Read();
		obj.TimedRequests();
	}
	
	this.TimedRequests = function()
	{
		obj.interval = setInterval(function(){obj.Read();}, obj.time);
	}
	
	this.Read = function()
	{
		this.Request = $.ajax(
		{
			url: "https://" + obj.url,
		});
		
		this.Request.done(function(data) 
		{
			obj.Boards(data);
		});

		this.Request.fail(function()
		{
			obj.ErrorMessage("Error reading API.");
		});
	}
	
	this.Boards = function(boards)
	{
		// check atleast BTC exists
		if (!boards.BTC) return;
		
		// clear prev
		$("section").html("");
		
		// show crypto boards
		$.each(boards, function(key, value)
		{
			obj.Board(key, value);
		});
	}
	
	this.Board = function(key, board)
	{
		// create board view
		var html = "<h2>" + key + "</h2>";
		
		$.each(board, function(key, value)
		{
			html += "<div class='tag'>" + key + "</div><div class='value'>" + value + "</div>";
		});
		
		$("section").append("<div class='board'>" + html + "</div>");
		
		// show last update
		$("#updates").html("Last update - " + new Date().toLocaleString());
	}
	
	// misc
	this.ErrorMessage = function(message)
	{
		window.alert(message);
	}
	
}
